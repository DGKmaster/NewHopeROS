#include <RoboLibL.h>
#include <TcpLib.h>

class URoboLibH
{
protected: // Глобальные переменные
    URoboLibL *RobotL;
    
protected: // Локальные переменные
    /// Направление движения
    int move_direction;
    
    /// Угол поворота
    int angle_direction;
    
public: 
    /// Конструкторы и деструкторы
    URoboLibH(void);
    virtual ~URoboLibH(void);
    
    /// Установка соединения
    int SetRobotL(URoboLibL *robot);
    
    /// Поворот на заданный угол
    int Turn(const int &angle);
    
    /// Задание направление движение
    /// 1 - вперёд, -1 - назад, 0 - остановка
    int MoveDirection(const int &move);
    
    /// 
};