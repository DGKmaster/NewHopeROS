#include "RoboLibL.h"

/// Конструкторы и деструкторы
UQueues::UQueues(void)
{
}

UQueues::~UQueues(void)
{
}

/// Добавление команды в очередь на отправку
/// в случае неудачи возвращает false
bool UQueues::PushToSendCommandQueue(const std::string &command)
{
    if(command.empty())
    {
        return false;
    }
    boost::mutex::scoped_lock lock(SendCommandQueueMutex);
    SendCommandQueue.push_back(command);

    return true;
}

/// Возвращает следующую на очереди команду из CommandQueue
/// и удаляет ее из очереди
std::string UQueues::PopFromSendCommandQueue(void)
{
    std::string result;
    boost::mutex::scoped_lock lock(SendCommandQueueMutex);
    if(SendCommandQueue.begin() != SendCommandQueue.end())
    {
        result = *(SendCommandQueue.begin());
        SendCommandQueue.erase(SendCommandQueue.begin());
    }
    return result;
}

/// Кладет ответ в очередь ответов
void UQueues::PushToReceiveCommandQueue(const std::string &command)
{
    boost::mutex::scoped_lock lock(ReceiveCommandQueueMutex);
    ReceiveCommandQueue.push_back(command);
}

/// Возвращает указатель на самый старый ответ
/// и удаляет его из очереди
std::string UQueues::PopFromReceiveCommandQueue(void)
{
    boost::mutex::scoped_lock lock(ReceiveCommandQueueMutex);
    std::string result;
    std::list<std::string>::iterator I = ReceiveCommandQueue.begin();

    if(ReceiveCommandQueue.begin() != ReceiveCommandQueue.end())
    {
        result = *(ReceiveCommandQueue.begin());
        ReceiveCommandQueue.erase(ReceiveCommandQueue.begin());
    }
    return result;
}

/// Очищает очередь команд
void UQueues::ClearSendQueue(void)
{
    boost::mutex::scoped_lock lock(SendCommandQueueMutex);
    SendCommandQueue.clear();
}

/// Очищает очередь ответов
void UQueues::ClearReceiveQueue(void)
{
    boost::mutex::scoped_lock lock(ReceiveCommandQueueMutex);
    ReceiveCommandQueue.clear();
}

/// Конструктор
URoboLibL::URoboLibL(void)
{
    Client = 0;
    FlagAnswer = 0;
    ControlNumber = 0;
    cmd_id = 0;
    RobotError = 0;
    TransportThreadTerminated = false;
    TransportThread = boost::thread(boost::bind(&URoboLibL::TransportThreadFunc, boost::ref(*this)));
}

/// Деструктор
URoboLibL::~URoboLibL(void)
{
    TransportThreadTerminated = true;
    TransportThread.join();
}

/// Установка клиента
int URoboLibL::SetClient(UTcpClient *client)
{
    Client = client;

    return 0;
}

/// Метод потока обработки очередей
void URoboLibL::TransportThreadFunc(void)
{
    int error = 0;
    // При ликвидации потока можем потерять часть команды
    std::string half_answer;

    while(!TransportThreadTerminated)
    {
        if(!Client)
        {
            boost::this_thread::sleep(boost::posix_time::milliseconds(1));
            continue;
        }

        std::string str = PopFromSendCommandQueue();
        if(!str.empty())
        {
            error = Client->SendStr(str);
        }
        error = Client->ReceiveStr(str);
        if(!str.empty())
        {
            // TODO: Тут надо парсить принимаемую строку, на случай если пришла только часть ответа, или 1.5 ответа.
            // Сейчас это не учтено.

            // Обработка строки на соответсвие протоколу комманд
            //
            // Поиск первого вхождения символа с в строке s. В случае удачного поиска возвращает
            // указатель на место первого вхождения символа с.
            // Если символ не найден, то возвращается ноль.
            // strchr(str.c_str(),'<');
            // strchr(str.c_str(),'>');
            PushToReceiveCommandQueue(str);
        }
        boost::this_thread::sleep(boost::posix_time::milliseconds(1));
    }
    if(error != 0)
    {
        std::cerr << "Error #" << error << std::endl;
    }
}

/// Ожидание ответа
bool URoboLibL::WaitResponse(std::string &response, unsigned int timeout)
{
    bool run_wait(true);

    boost::posix_time::ptime start_wait_time = boost::posix_time::microsec_clock::local_time();

    while(run_wait)
    {
        response = PopFromReceiveCommandQueue();
        if(!response.empty())
        {
            return true;
        }
        boost::posix_time::ptime t1 = boost::posix_time::microsec_clock::local_time();
        boost::posix_time::time_duration diff;
        diff = t1 - start_wait_time;
        unsigned long long real_fps_diff = diff.total_milliseconds();
        if(real_fps_diff > timeout)
        {
            run_wait = false;
            break;
        }

        boost::this_thread::sleep(boost::posix_time::milliseconds(1));
    }
    return false;
}

/// Возвращает строку с номером команды
/// и увеличивает на 1
char *URoboLibL::ReturnCmdId()
{
    /// Перевод int to string
    sprintf(cmd_id_to_str, "%d", cmd_id);
    // TODO Исправить первые команды, которые должны
    // быть вида 00 01 02
    ++cmd_id;
    return cmd_id_to_str;
}

/// Запрос ID устройства
int URoboLibL::GetID(int &id)
{
    /// Локальные переменные
    std::string send_str;

    /// Подготовка сообщения
    send_str += "<";
    send_str += ReturnCmdId();
    send_str += " GET_ID>";

    /// Отправка сообщения
    PushToSendCommandQueue(send_str);

    return 0;
}

/// Выключение питания
int URoboLibL::TurnOff(const unsigned int &seconds)
{
    /// Локальные переменные
    std::string send_str;
    char int_to_str[10];

    /// Перевод int to string
    sprintf(int_to_str, "%d", seconds);

    /// Подготовка сообщения
    send_str += "<";
    send_str += ReturnCmdId();
    send_str += "OFF ";
    send_str += int_to_str;
    send_str += ">";

    /// Отправка сообщения
    PushToSendCommandQueue(send_str);

    return 0;
}

/// Тест звука
int URoboLibL::TestSound()
{
    /// Локальные переменные
    std::string send_str;

    /// Подготовка сообщения
    send_str += "<";
    send_str += ReturnCmdId();
    send_str += "R2D2>";

    /// Отправка сообщения
    PushToSendCommandQueue(send_str);
    return 0;
}

/// Парсер для полученных ответов
/// Возвращает 0, если ответ понят
int URoboLibL::Parser(const std::string &command)
{
    // TODO: Нерабочий вариант находится в проекте Test
    // необходимо его доработать используя следующую рекомендацию
    // выделение подстроки substr(индекс начала, размер)
    // размер можно получить из разности первого поиска и второго.
    return 0;
}

/*
    std::string response;
    
    // Вариант ожидания ответа
    if(WaitResponse(response, 2))
    {
        id = atoi(response.c_str()); // TODO: Тут нет предварительной проверки на то,
        // что response можно превращать в id, и нет парсинга ответа (предполагается, что там прямо сам id пришел, без
        // служебной инфы).
        return 0;
    }
*/