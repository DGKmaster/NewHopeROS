#ifndef ROBOLIBL_H
#define ROBOLIBL_H

#include <TcpLib.h>
#include <boost/thread.hpp>
#include <boost/thread/mutex.hpp>

/// Системные ошибки
#define Err_InvalidChar 0
#define Err_UnknownCmd 1
#define Err_TimeStamp 2
#define Err_Param 3
#define Err_ParamValue 4
#define Err_WorkMode 5

/// Реализует управление очередями команд
class UQueues
{
protected:
    /// Очередь команд, ожидающих обработки
    std::list<std::string> SendCommandQueue;

    /// Очередь ответов на команды, ожидающих отправки
    std::list<std::string> ReceiveCommandQueue;

protected: // Потоки
    /// Мьютекс дня блокировки доступа к очереди команд
    boost::mutex SendCommandQueueMutex;

    /// Мьютекс дня блокировки доступа к очереди ответов
    boost::mutex ReceiveCommandQueueMutex;

public:
    /// Конструкторы и деструкторы
    UQueues(void);
    virtual ~UQueues(void);

    /// Добавление команды в очередь на обработку
    /// в случае неудачи возвращает false
    virtual bool PushToSendCommandQueue(const std::string &command);

    /// Возвращает следующую на очереди команду из CommandQueue
    /// и удаляет ее из очереди
    virtual std::string PopFromSendCommandQueue(void);

    /// Кладет ответ в очередь ответов
    virtual void PushToReceiveCommandQueue(const std::string &command);

    /// Возвращает указатель на самый старый ответ
    /// и удаляет его из очереди
    virtual std::string PopFromReceiveCommandQueue(void);

    /// Очищает очередь команд
    virtual void ClearSendQueue(void);

    /// Очищает очередь ответов
    virtual void ClearReceiveQueue(void);
};

class URoboLibL : public UQueues
{
protected: // Глобальные переменные
    /// Содержит сведения о сервере
    UTcpClient *Client;

    /// Поток обработки очередей команд
    boost::thread TransportThread;

    /// Флаг работы потока
    bool TransportThreadTerminated;

    /// Равен 1 в случае запроса робота
    /// на контроль соединения
    int FlagAnswer;

    /// Номер контрольного соединения
    int ControlNumber;

    /// Принятая ошибка робота
    int RobotError;

protected: // Локальные переменные
    /// Номер команды, начиная с 00
    int cmd_id;

    /// Для перевода номера команды
    /// в строку
    char cmd_id_to_str[10];

protected:
    /// Метод потока обработки очередей команд
    void TransportThreadFunc(void);

    /// Ожидает ответ
    bool WaitResponse(std::string &response, unsigned int timeout);

public:
    /// Конструкторы и деструкторы
    URoboLibL(void);
    virtual ~URoboLibL(void);

    /// Установка клиента
    int SetClient(UTcpClient *client);

public: // Команды на отправку
    /// Запрос ID устройства
    int GetID(int &id);

    /// Выключение питания
    int TurnOff(const unsigned int &seconds);

    /// Тест звука
    int TestSound();

    /// Ответ на контроль соединения
    /// Без ответа принятые команды не будут
    /// исполняться
    int ControlAnswer(const unsigned int &number);

    /// Установка мощности на один из двигателей
    int SetPWM(const unsigned int &drive_num, const int &pwm_value);

    /// Парсер для полученных ответов
    /// Возвращает 0, если ответ понят
    int Parser(const std::string &command);

    /// Возвращает строку с номером команды
    /// и увеличивает на 1
    char *ReturnCmdId();
};

#endif