#include <TcpLib.h>

int main(int argc, char* argv[])
{
    UTcpClient Client;
    std::vector<char> buffer;
    buffer.assign(255, 0);
    int flag;
    
    if (argc < 3) {
       fprintf(stderr,"usage %s hostname port\n", argv[0]);
       exit(1);
    }
    
    // flag = Client.SetServerAddress("192.168.1.138");
    flag = Client.SetServerAddress(argv[1]);
    std::cout << flag << std::endl;

    // flag = Client.SetServerPort(5761);
    flag = Client.SetServerPort(atoi(argv[2]));
    std::cout << flag << std::endl;

    flag = Client.Connect();
    std::cout << flag << std::endl;

    while(true)
    {
        std::cout << "Please enter the message: ";
        std::string str;
        std::cin >> str;
        // bzero(buffer,buffer.size());
        // fgets(&buffer[0],255,stdin);

        flag = Client.SendStr(str);
        std::cout << flag << std::endl;

        // bzero(buffer,256);

        std::string str2;
        flag = Client.ReceiveStr(str2);
        std::cout << "\n Here is the message: " << str2 << std::endl;
        std::cout << flag << std::endl;
    }
    flag = Client.Disconnect();
    std::cout << flag << std::endl;

    return 0;
}
