#include <TcpLib.h>

int main(int argc, char* argv[])
{
    UTcpServer Server;
    std::vector<char> buffer;
    int flag;
    std::string str;
    
    if (argc < 2) {
         fprintf(stderr,"ERROR, no port provided\n");
         exit(1);
     }
    
    flag = Server.SetPort(atoi(argv[1]));
    std::cout << flag << std::endl;

    flag = Server.ListenOn();
    std::cout << flag << std::endl;

    while(true)
    {
        flag = Server.ReceiveStr(str);
        std::cout << "\n Here is the message: " << str << std::endl;
        // usleep(1000);

        std::cout << flag << std::endl;

        flag = Server.SendStr(str);
        std::cout << flag << std::endl;
    }
    flag = Server.ListenOff();
    std::cout << flag << std::endl;

    return 0;
}