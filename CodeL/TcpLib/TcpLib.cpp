#include "TcpLib.h"

/// Конструкторы
UTcpBase::UTcpBase(void)
{
    LastError = TCP_EXCHANGE_NO_ERROR;
    ExchangeBuffer.resize(255);
}

/// Вывод последней ошибки
int UTcpBase::GetLastError(void) const
{
    return LastError;
}

int UTcpBase::ReceiveBuf(std::vector<char>& buffer)
{
    buffer.clear();

    int read_bytes(0);
    int err = ReceiveBufRaw(ExchangeBuffer, read_bytes);

    if(err != 0)
    {
        LastError = err;
        return err;
    }

    buffer.resize(read_bytes);
    if(read_bytes > 0)
    {
        memcpy(&buffer[0], &ExchangeBuffer[0], read_bytes * sizeof(char));
    }

    return 0;
}

int UTcpBase::ReceiveStr(std::string& str)
{
    str.clear();

    int read_bytes(0);
    int err = ReceiveBufRaw(ExchangeBuffer, read_bytes);

    if(err != 0)
    {
        LastError = err;
        return err;
    }

    str.resize(read_bytes);
    if(read_bytes > 0)
    {
        memcpy(&str[0], &ExchangeBuffer[0], read_bytes * sizeof(char));
    }

    return 0;
}

int UTcpBase::ReceiveBufRaw(std::vector<char>& buffer, int& read_bytes)
{
    // Read using new file descriptor either the total number of
    // characters in the socket or 255.
    n = read(newsockfd, &buffer[0], buffer.size());
    if(n < 0)
    {
        read_bytes = 0;
        LastError = SOCKET_READING_ERROR;
        return LastError;
    }
    read_bytes = n;
    return 0;
}

int UTcpBase::SendBuf(const std::vector<char>& buffer)
{
    if(buffer.empty())
    {
        LastError = NOTHING_TO_SEND;
        return NOTHING_TO_SEND;
    }

    return SendBufRaw(&buffer[0], buffer.size());
}

int UTcpBase::SendStr(const std::string& str)
{
    return SendBufRaw(str.c_str(), str.size());
}

int UTcpBase::SendBufRaw(const char* data, int size)
{
    if(!data || size <= 0)
    {
        LastError = NOTHING_TO_SEND;
        return NOTHING_TO_SEND;
    }

    n = write(newsockfd, data, size);
    if(n < 0)
    {
        LastError = SOCKET_WRITING_ERROR;
        return LastError;
    }
    return 0;
}

UTcpClient::UTcpClient(void)
{
    /// Очистка буффера
    bzero((char*)&serv_addr, sizeof(serv_addr));
    // ExchangeBuffer.clear();

    /// Contains a code for the address family
    serv_addr.sin_family = AF_INET;
}

int UTcpClient::Connect(void)
{
    /// Создание сокета и определение его типа
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    newsockfd = sockfd;

    if(sockfd < 0)
    {
        LastError = ERROR_OPENING_SOCKET;
        return LastError;
    }

    if(connect(sockfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0)
    {
        LastError = CONNECT_ERROR;
        return LastError;
    }
    return 0;
}

int UTcpClient::SetServerAddress(const std::string& str)
{
    server = gethostbyname(str.c_str());

    if(server == NULL)
    {
        LastError = NO_SUCH_HOST;
        return LastError;
    }

    bcopy((char*)server->h_addr, (char*)&serv_addr.sin_addr.s_addr, server->h_length);

    return 0;
}

int UTcpClient::SetServerPort(const unsigned int& portnum)
{
    ServerPort = portnum;
    /// Convert int to network byte order
    serv_addr.sin_port = htons(ServerPort);

    return 0;
}

int UTcpClient::Disconnect(void)
{
    close(sockfd);

    return 0;
}

UTcpServer::UTcpServer(void)
{
    /// Очистка буффера
    bzero((char*)&serv_addr, sizeof(serv_addr));
    // ExchangeBuffer.clear();

    /// Contains a code for the address family
    serv_addr.sin_family = AF_INET;

    /// Get IP address of host machine
    serv_addr.sin_addr.s_addr = INADDR_ANY;
}

int UTcpServer::SetPort(const unsigned int& portnum)
{
    ListenPort = portnum;
    /// Convert int to network byte order
    serv_addr.sin_port = htons(ListenPort);
    return 0;
}

int UTcpServer::ListenOn(void)
{
    /// Создание сокета и определение его типа
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if(sockfd < 0)
    {
        LastError = ERROR_OPENING_SOCKET;
        return LastError;
    }

    /// Binds a socket to an address of the current host and port number on which the server will run
    if(bind(sockfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0)
    {
        LastError = SOCKET_BINDING_ERROR;
        return LastError;
    }

    // Listen on the socket for connections
    listen(sockfd, 5);

    clilen = sizeof(cli_addr);

    // Causes the process to block until a client connects
    // Wakes up the process when a connection has been established
    // Returns a new file descriptor, and all communication on this
    // connection should be done using the new file descriptor.
    // Second argument - reference pointer to client address
    // Third argument - size of structure
    newsockfd = accept(sockfd, (struct sockaddr*)&cli_addr, &clilen);

    if(newsockfd < 0)
    {
        LastError = ACCEPT_ERROR;
        return LastError;
    }
    return 0;
}

int UTcpServer::ListenOff(void)
{
    close(newsockfd);
    close(sockfd);

    return 0;
}
