#ifndef TCPLIB_H
#define TCPLIB_H

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string>
#include <unistd.h>
#include <vector>

// Contains definitions of a number of data types used in
// system calls. These types are used in the next two include files.
#include <sys/types.h>

// Includes a number of definitions of structures needed for sockets.
#include <sys/socket.h>

// Contains constants and structures needed for internet domain addresses.
#include <netinet/in.h>

// defines the structure hostent
#include <netdb.h>

/// Коды ошибок
#define TCP_EXCHANGE_NO_ERROR 0
#define ERROR_OPENING_SOCKET 1
#define INCORRECT_SERVER_PORT 2
#define SOCKET_BINDING_ERROR 3
#define ACCEPT_ERROR 4
#define SOCKET_READING_ERROR 5
#define SOCKET_WRITING_ERROR 6
#define NO_SUCH_HOST 7
#define CONNECT_ERROR 8
#define NOTHING_TO_SEND 9

class UTcpBase
{
protected: // Глобальные данные обмена
           /// Код ошибки последней операции
    int LastError;

    /// Буфер обмена
    std::vector<char> ExchangeBuffer;

protected: // Внутренние данные обмена
/// Файловый дескриптор для созданного сокета
    int sockfd;
    int newsockfd;

    /// Количество принятых/отправленных символов
    int n;

public:
    /// Конструкторы
    UTcpBase(void);

    /// Код ошибки последней операции
    int GetLastError(void) const;

    /// Отправка данных
    int SendBuf(const std::vector<char>& buffer);
    int SendStr(const std::string& str);

    /// Прием данных
    /// Возвращает код ошибки
    int ReceiveBuf(std::vector<char>& buffer);
    int ReceiveStr(std::string& str);

protected:
    int SendBufRaw(const char* data, int size);
    int ReceiveBufRaw(std::vector<char>& buffer, int& read_bytes);
};

class UTcpServer : public UTcpBase
{
protected: // Глобальные данные обмена
           /// Слушающий порт для входящего подключения
    unsigned int ListenPort;

protected: // Внутренние данные обмена
           /// Size of the address of the client
    socklen_t clilen;

    /// New socket for client connection
    // int newsockfd;
    // int sockfd;

    /// Structure containing an internet address
    sockaddr_in serv_addr, cli_addr;

public:
    /// Конструкторы
    UTcpServer(void);

    /// Подключение
    int ListenOn(void);

    /// Отключение
    int ListenOff(void);

    /// Создание сокета путём соединения адреса хост машины
    /// и переданного номера порта в качестве аргумента
    int SetPort(const unsigned int& portnum);
};

class UTcpClient : public UTcpBase
{
protected: // Глобальные данные обмена
           /// Адрес сервера
    std::string ServerAddr;

    /// Порт клиента
    unsigned int ServerPort;

protected: // Внутренние данные обмена
    struct sockaddr_in serv_addr;
    struct hostent* server;

public:
    /// Конструкторы
    UTcpClient(void);

    /// Подключение
    int Connect(void);

    /// Отключение
    int Disconnect(void);

    /// Установка адреса сервера
    int SetServerPort(const unsigned int& portnum);
    int SetServerAddress(const std::string& str);
};

#endif
